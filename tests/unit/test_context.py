import mock
import pytest

import context


@pytest.mark.parametrize(
    "environment, validate, expected",
    [
        ({}, False, context.Context()),
        ({"CTX_PROJECT": "project"}, True, context.Context(project="project")),
        (
            {"CTX_PROJECT": "project", "CTX_SEQUENCE": "", "CTX_SHOT": "shot"},
            False,
            context.Context(project="project", shot="shot"),
        ),
        (
            {"CTX_PROJECT": "project", "CTX_SEQUENCE": "", "CTX_SHOT": ""},
            True,
            context.Context(project="project"),
        ),
        (
            {
                "CTX_TASK": "task",
                "CTX_SEQUENCE": "sequence",
                "CTX_PROJECT": "project",
                "CTX_SHOT": "shot",
            },
            True,
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
        ),
    ],
)
@mock.patch("context.os")
def test_from_environment(mock_os, environment, validate, expected):
    mock_os.getenv.side_effect = lambda x: environment.get(x)
    assert context.Context.from_environment(validate=validate) == expected


@pytest.mark.parametrize(
    "fields, validate, expected",
    [
        ({}, False, context.Context()),
        ({"project": "project"}, True, context.Context(project="project")),
        (
            {"project": "project", "sequence": None, "shot": None},
            True,
            context.Context(project="project"),
        ),
        (
            {"project": "project", "sequence": None, "shot": "shot"},
            False,
            context.Context(project="project", shot="shot"),
        ),
        (
            {
                "task": "task",
                "sequence": "sequence",
                "project": "project",
                "shot": "shot",
            },
            True,
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
        ),
    ],
)
def test_from_fields(fields, validate, expected):
    assert context.Context.from_fields(fields, validate=validate) == expected


@pytest.mark.parametrize(
    "iterable, validate, expected",
    [
        ([], False, context.Context()),
        (["project"], True, context.Context(project="project")),
        (["project", None, None], True, context.Context(project="project")),
        (
            ["project", None, "shot"],
            False,
            context.Context(project="project", shot="shot"),
        ),
        (
            iter(("project", "sequence", "shot", "task")),
            True,
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
        ),
    ],
)
def test_from_iterable(iterable, validate, expected):
    assert context.Context.from_iterable(iterable, validate=validate) == expected


@pytest.mark.parametrize(
    "string, validate, expected",
    [
        ("/", False, context.Context()),
        ("///", False, context.Context()),
        ("/project/", True, context.Context(project="project")),
        ("/project//shot", False, context.Context(project="project", shot="shot")),
        ("/project//", True, context.Context(project="project")),
        (
            "/project/sequence/shot/task",
            True,
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
        ),
    ],
)
def test_from_string(string, validate, expected):
    assert context.Context.from_string(string, validate=validate) == expected


@pytest.mark.parametrize(
    "ctx, string",
    [
        (context.Context(), ""),
        (context.Context(project="project"), "/project"),
        (context.Context(project="project", shot="shot"), "/project//shot"),
        (
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
            "/project/sequence/shot/task",
        ),
    ],
)
def test_str(ctx, string):
    assert str(ctx) == string


@pytest.mark.parametrize(
    "ctx, string",
    [
        (
            context.Context(),
            "Context(project=None, sequence=None, shot=None, task=None)",
        ),
        (
            context.Context(project="project"),
            "Context(project='project', sequence=None, shot=None, task=None)",
        ),
        (
            context.Context(project="project", shot="shot"),
            "Context(project='project', sequence=None, shot='shot', task=None)",
        ),
        (
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
            "Context(project='project', sequence='sequence', shot='shot', task='task')",
        ),
    ],
)
def test_repr(ctx, string):
    assert repr(ctx) == string


@pytest.mark.parametrize(
    "ctx_a, ctx_b, is_equal",
    [
        (context.Context(), context.Context(), True),
        (context.Context(project="project"), context.Context(project="project"), True),
        (
            context.Context(project="project", shot="shot"),
            context.Context(project="project", shot="shot"),
            True,
        ),
        (
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
            True,
        ),
        (context.Context(), context.Context(project="project"), False),
    ],
)
def test_eq(ctx_a, ctx_b, is_equal):
    assert (ctx_a == ctx_b) is is_equal


@pytest.mark.parametrize(
    "ctx_a, ctx_b, is_equal",
    [
        (context.Context(), context.Context(), True),
        (context.Context(project="project"), context.Context(project="project"), True),
        (
            context.Context(project="project", shot="shot"),
            context.Context(project="project", shot="shot"),
            True,
        ),
        (
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
            True,
        ),
        (context.Context(), context.Context(project="project"), False),
    ],
)
def test_hash(ctx_a, ctx_b, is_equal):
    assert (hash(ctx_a) == hash(ctx_b)) is is_equal


def test_project():
    ctx = context.Context(project="project")
    assert ctx.project == "project"


def test_sequence():
    ctx = context.Context(sequence="sequence")
    assert ctx.sequence == "sequence"


def test_shot():
    ctx = context.Context(shot="shot")
    assert ctx.shot == "shot"


def test_task():
    ctx = context.Context(task="task")
    assert ctx.task == "task"


@pytest.mark.parametrize(
    "ctx, values_only, expected_fields",
    [
        (context.Context(project="project"), True, {"project": "project"}),
        (
            context.Context(project="project"),
            False,
            {"project": "project", "sequence": None, "shot": None, "task": None},
        ),
        (
            context.Context(project="project", shot="shot"),
            True,
            {"project": "project", "shot": "shot"},
        ),
        (
            context.Context(project="project", shot="shot"),
            False,
            {"project": "project", "sequence": None, "shot": "shot", "task": None},
        ),
        (context.Context(), True, {}),
        (
            context.Context(),
            False,
            {"project": None, "sequence": None, "shot": None, "task": None},
        ),
    ],
)
def test_fields(ctx, values_only, expected_fields):
    assert ctx.fields(values_only=values_only) == expected_fields


@pytest.mark.parametrize(
    "ctx, is_valid",
    [
        (context.Context(project="project"), True),
        (context.Context(project="project", shot="shot"), False),
        (context.Context(), False),
    ],
)
def test_is_valid(ctx, is_valid):
    assert ctx.is_valid() == is_valid


@pytest.mark.parametrize(
    "ctx, expected_level",
    [
        (context.Context(project="project", sequence=None, shot="shot"), "shot"),
        (context.Context(project="project"), "project"),
        (context.Context(), None),
    ],
)
def test_level(ctx, expected_level):
    assert ctx.level() == expected_level


@pytest.mark.parametrize(
    "ctx, expected_parent",
    [
        (
            context.Context(project="project", sequence="sequence", shot="shot"),
            context.Context(project="project", sequence="sequence"),
        ),
        (
            context.Context(project="project", sequence=None, shot="shot"),
            context.Context(project="project"),
        ),
        (context.Context(project="project"), context.Context()),
        (context.Context(), context.Context()),
    ],
)
def test_parent(ctx, expected_parent):
    assert ctx.parent() == expected_parent


@pytest.mark.parametrize(
    "existing, ctx, result",
    [
        (
            {"CTX_SHOT": "shot"},
            context.Context(project="project", sequence="sequence"),
            {"CTX_PROJECT": "project", "CTX_SEQUENCE": "sequence"},
        ),
        ({"CTX_PROJECT": "project", "CTX_SHOT": "shot"}, context.Context(), {}),
        (
            {"CTX_TASK": "other"},
            context.Context(
                project="project", sequence="sequence", shot="shot", task="task"
            ),
            {
                "CTX_PROJECT": "project",
                "CTX_SEQUENCE": "sequence",
                "CTX_SHOT": "shot",
                "CTX_TASK": "task",
            },
        ),
    ],
)
@mock.patch("context.os")
def test_set(mock_os, existing, ctx, result):
    mock_os.environ = existing
    ctx.set()
    assert mock_os.environ == result


@pytest.mark.parametrize(
    "ctx", [context.Context(project="project", task="task"), context.Context()]
)
def test_validate(ctx):
    with pytest.raises(context.ContextError):
        ctx.validate()
