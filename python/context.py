import collections
import os


class ContextError(Exception):
    """ Errors with context """


class Context(object):
    """
    Context represents a specific environment based on an ordered set of fields
    and values. A Context only stores the minimum number of fields required to
    represent it. Never initialise a Context directly, use the constructor
    methods instead.
    """

    FIELDS = ("project", "sequence", "shot", "task")
    VARS = tuple("CTX_" + f.upper() for f in FIELDS)

    @classmethod
    def from_environment(cls, validate=True):
        """
        Raises:
            ContextError: If validate is True and values are invalid

        Keyword Args:
            validate (bool): Whether or not to validate the values

        Returns:
            Context: Context created from the current environment variables
        """
        ctx = cls(*(os.getenv(var) for var in cls.VARS))
        if validate:
            ctx.validate()
        return ctx

    @classmethod
    def from_fields(cls, fields, validate=True):
        """
        Raises:
            ContextError: If validate is True and values are invalid

        Args:
            fields (dict[str, str]): Dictionary of fields mapped to values

        Keyword Args:
            validate (bool): Whether or not to validate the values

        Returns:
            Context: Context from a dictionary of field values
        """
        ctx = cls(
            project=fields.get("project"),
            sequence=fields.get("sequence"),
            shot=fields.get("shot"),
            task=fields.get("task"),
        )
        if validate:
            ctx.validate()
        return ctx

    @classmethod
    def from_iterable(cls, values, validate=True):
        """
        Args:
            values (Iterable[str]): Any iterable of values to be paired against
                the ordered fields

        Keyword Args:
            validate (bool): Whether or not to validate the values

        Returns:
            Context: Context from pairing fields and values
        """
        ctx = cls(*values)
        if validate:
            ctx.validate()
        return ctx

    @classmethod
    def from_string(cls, string, validate=True):
        """
        Args:
            string (str): A string matching str(Context)

        Keyword Args:
            validate (bool): Whether or not to validate the values

        Returns:
            Context: Context parsed from a context string
        """
        values = string.strip("/").split("/")
        ctx = cls(*values)
        if validate:
            ctx.validate()
        return ctx

    def __init__(self, project=None, sequence=None, shot=None, task=None):
        """
        Args:
            project (str|None):
            sequence (str|None):
            shot (str|None):
            task (str|None):
        """
        # Only populate all sequential fields to the deepest provided value
        values = []
        for val in (task, shot, sequence, project):
            if val or values:
                values.append(val or None)

        self._fields = collections.OrderedDict(zip(self.FIELDS, reversed(values)))

    def __str__(self):
        values = [""]
        values.extend(value or "" for value in self._fields.values())
        return "/".join(values)

    def __repr__(self):
        return (
            "{ctx.__class__.__name__}(project={ctx.project!r}, "
            "sequence={ctx.sequence!r}, shot={ctx.shot!r}, task={ctx.task!r})".format(
                ctx=self
            )
        )

    def __eq__(self, other):
        return isinstance(other, Context) and self._fields == other._fields

    def __ne__(self, other):
        return self != other

    def __hash__(self):
        return hash(tuple(self._fields.values()))

    @property
    def project(self):
        """
        Returns:
            str: Project value
        """
        return self._fields.get("project")

    @property
    def sequence(self):
        """
        Returns:
            str: Sequence value
        """
        return self._fields.get("sequence")

    @property
    def shot(self):
        """
        Returns:
            str: Shot value
        """
        return self._fields.get("shot")

    @property
    def task(self):
        """
        Returns:
            str: Task value
        """
        return self._fields.get("task")

    def fields(self, values_only=False):
        """
        Keyword Args:
            values_only (bool):

        Returns:
            dict[str, str|None]: Dictionary mapping field names to values
        """
        if values_only:
            fields = {field: value for field, value in self._fields.items() if value}
        else:
            fields = {field: None for field in self.FIELDS}
            fields.update(self._fields)
        return fields

    def is_valid(self):
        """
        Returns:
            bool: Validate() as a boolean value
        """
        try:
            self.validate()
        except ContextError:
            return False
        return True

    def level(self):
        """
        Returns:
            str: Field name for the deepest valid Context value
        """
        return tuple(self._fields.keys())[-1] if self._fields else None

    def parent(self):
        """
        Returns:
            Context: Next valid context after removing the lowest level field
        """
        if not self._fields:
            return self.__class__()

        clone = self._fields.copy()
        clone.popitem()
        return self.__class__(**clone)

    def set(self):
        """ Sets the context as the current environment """
        values = list(self._fields.values())
        values.extend([None] * (len(self.FIELDS) - len(values)))
        for var, value in zip(self.VARS, values):
            if value:
                os.environ[var] = value
            elif var in os.environ:
                os.environ.pop(var, None)

    def validate(self):
        """ Runs the validation checks and raises any ContextErrors """
        if not self._fields:
            raise ContextError("Empty context")
        if not all(self._fields.values()):
            raise ContextError("Invalid context field order: {}".format(self))
